package de.jw.meshserializer;

import java.util.Arrays;
import java.util.List;

public class Constants {
	
	public static final String PLUGIN_ID = "de.jw.meshserializer";
	
	public static final List<String> supportedExtensions = Arrays.asList("3ds","obj","jaw","asc","md2");
	
}
